using System.Collections.Generic;
using System;

namespace TwitterApi.Models
{
    class UserRepository : IUserRepository
    {
        private readonly TwitterContext _context;

        public UserRepository(TwitterContext context)
        {
            _context = context;
        }
        public void Add(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public User FindByUserID(string key)
        {
            Console.WriteLine("Inside");
            int k = Int32.Parse(key);
            foreach(User user in _context.Users) {
                Console.WriteLine(user.UserID + "==" + key);
                if(user.UserID == k) {
                    return user;
                }
            }
            return null;
        }

        public User FindByName(string name)
        {
            foreach(User user in _context.Users) {
                if(user.Name == name) {
                    return user;
                }
            }
            return null;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public User Remove(int key)
        {
            User user = FindByUserID(key.ToString());
            _context.Users.Remove(user);
            _context.SaveChanges();
            return user;
        }

        public void Update(User user)
        {
            _context.Users.Update(user);
            _context.SaveChanges();
        }
    }
}