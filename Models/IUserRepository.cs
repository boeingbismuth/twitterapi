using System.Collections.Generic;

namespace TwitterApi.Models
{

    public interface IUserRepository
    {
        void Add(User user);
        IEnumerable<User> GetAll();
        User FindByUserID(string id);
        User FindByName(string name);
        User Remove(int key);
        void Update(User user);
        
    }
}